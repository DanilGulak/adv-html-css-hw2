const burgerMenuToggle = document.querySelector('.burger');
burgerMenuToggle.addEventListener('click', () => burgerMenuToggle.classList.toggle('burger--opened'));